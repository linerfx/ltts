#include "MainWindow.h"
#include <QApplication>
#include <QLocale>
#include <QTranslator>
//#include <csignal>
#include "../../common/io_path.h"
#include "Options.h"
#include "../../common/ErrLog.h"

void on_signal(int n);
extern COptions g_opt;
namespace err {
extern CLog log;
}// namespace err
extern glb::CFxArray<COfst, true> g_ao;	// файловые отступы для быстрого перехода по пройденным параграфам
extern int32_t g_iCurOfst;				// индекс тек.эл-та в g_ao

bool OnInit();// иницыацыя см. global.h
void AtExit();// вызывается при завершении приложения

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QTranslator translator;
	const QStringList uiLanguages = QLocale::system().uiLanguages();
	for (const QString &locale : uiLanguages) {
		const QString baseName = "ltts_" + QLocale(locale).name();
		if (translator.load(":/i18n/" + baseName)) {
			a.installTranslator(&translator);
			break;
		}
	}// на выходе сохраню настройки ещё разок
	atexit(AtExit);
// "сигналы" из с-библиотеки
//	signal(SIGABRT, on_signal);
	try {// иницыацыя
		fl::CPath::CreateAppPath("ltts");
		OnInit();
		// главное окно
		CMainWindow w;
		w.show();
		return a.exec();
	} catch (...) {
		err::log.Add("%t Исключение в main().", time(NULL));
		return 1;
	}
}
