#include "MainWindow.h"
#include "./ui_MainWindow.h"
#include <QFileDialog>
#include <QStandardPaths>
#include "DlgEngVoice.h"
#include <QPlainTextEdit>
#include "../../common/FxString.h"
#include "Options.h"

namespace glb {
thread_local CFxString sAppPath;
const char* FileName(const char *pcszPath, const int cPathLen);
}
glb::CFxArray<COfst, true> g_ao(128);// file offsets отступы для быстрого перехода по пройденным параграфам
int32_t g_iCurOfst = -1;	// индекс тек.эл-та в g_ao
glb::CFxArray<char, true> g_ac(128);// это динамич. буфер. К-во символов должно быть равно к-ву выд.памяти
FILE *g_pf = NULL;
long int g_nFileLen;// размер файла в байтах
int TrimString();
QMainWindow *g_pmw;
extern COptions g_opt;

CMainWindow::CMainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::CMainWindow)
	, m_ptts(nullptr)
	, m_pspkr(nullptr)	//(*(m_ptts = new QTextToSpeech(this)))
	, m_pDlg(nullptr)
{
	ui->setupUi(this);
	g_pmw = this;
	ReinitTts();
	QMetaObject::Connection cn;
	// команды меню
	QAction *pa = findChild<QAction*>("aOpen");
	cn = connect(pa, &QAction::triggered, this, &CMainWindow::OnOpenFile);
	assert((bool) cn == true);
	pa = findChild<QAction*>("aPlay");
	cn = connect(pa, &QAction::triggered, this, &CMainWindow::OnPlayOrStop);
	assert((bool) cn == true);
	pa = findChild<QAction*>("aStop");
	pa->setEnabled(false); // кнопка "остановить" отключена
	cn = connect(pa, &QAction::triggered, this, &CMainWindow::OnPlayOrStop);
	assert((bool) cn == true);
	pa = findChild<QAction*>("aEngVoce");
	cn = connect(pa, &QAction::triggered, this, &CMainWindow::OnChangeSettings);
	assert((bool) cn == true);
	QPlainTextEdit *pte = findChild<QPlainTextEdit*>("plainTextEdit");
	pte->setWordWrapMode(QTextOption::WordWrap);
	pa = findChild<QAction*>("aNext");
	cn = connect(pa, &QAction::triggered, this, &CMainWindow::OnReadNext);
	assert((bool) cn == true);
	pa = findChild<QAction*>("aPrev");
	cn = connect(pa, &QAction::triggered, this, &CMainWindow::OnReadPrevious);
//	pa->setEnabled(false); // кнопка "назад" изначально отключена
	assert((bool) cn == true);
	SetCmdEnabled("aPrev", false);
	SetCmdEnabled("aNext", false);
	// применю настройки, если есть
	if (g_opt.IsThereAnyVoice())
		m_pspkr->SetEng(g_opt.CurVoice());
	// если в настройках достаточно сведений, чтобы начать чтение, начну читать
	if (g_opt.IsThereAFiletoRead())
		OnSettingsChanged(g_opt.m_nRow);
}

CMainWindow::~CMainWindow()
{
	delete ui;
	delete m_pspkr;
	delete m_ptts;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CMainWindow::OpenFile открываю текстовый файл
/// \param pcszPn: указатель на путь и имя файла. Если нуль, выведу диалог "открыть текстовый файл"
/// \return
///
bool CMainWindow::OpenFile(const char *pcszPn) // pcszPn = nullptr
{
	if (!pcszPn) {
		QStringList as = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
		QString sfn = QFileDialog::getOpenFileName(this, "Открыть файл для чтения", as.first(),
												   "Текст: (*.txt *.TXT)").trimmed();
		FILE *pf = NULL;
		if (!sfn.length())
			return false;
		g_opt.m_sfp = qPrintable(sfn);
		pcszPn = g_opt.m_sfp;
		if ((pf = g_opt.OpenFile()) != NULL)
			g_opt.PutFile(pf); // закроет файл по завершении записи
	}// открываю текстовый файл
	errno = 0;
	g_pf = fopen(pcszPn, "r");
	if (!g_pf) {
		QStatusBar *psb = g_pmw->statusBar();
		psb->showMessage(QString("Невозможно открыть указанный файл: %1").arg(strerror(errno)));
		return false;
	}
	setWindowTitle(QString("Читаю ") + glb::FileName(g_opt.m_sfp, g_opt.m_sfp.GetCount()));
	if (fseek(g_pf, 0, SEEK_END) != 0 ||
		(g_nFileLen = ftell(g_pf)) == -1 ||
		fseek(g_pf, 0, SEEK_SET) != 0) {
		QStatusBar *psb = g_pmw->statusBar();
		psb->showMessage(QString("ошибка работы с файлом: %1").arg(strerror(errno)));
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CMainWindow::SetCmdEnabled делаю состояние указанной команды (action)
/// \param cmdName: имя команды (action)
///
void CMainWindow::SetCmdEnabled(const char *cmdName, bool bEnable)
{
	QAction *pa = findChild<QAction*>(cmdName);
	pa->setEnabled(bEnable);
}

void CMainWindow::OnOpenFile(bool)
{
	if (g_pf) {
		fclose(g_pf);
		g_pf = NULL;
	}
	if (g_iCurOfst > 0) {
		ReinitTts();
		g_iCurOfst = 0;
		g_ao.SetCountND(1);
		ASSERT(g_ao[0].nOfst == 0 && g_ao[0].nRow == 1);
	}
	OnPlayOrStop(true);
}

void CMainWindow::OnPlayOrStop(bool)
{
	ASSERT(m_ptts);
	if (!g_pf && !OpenFile())
		return;
	bool breading = m_ptts->state() == QTextToSpeech::Speaking;
	if (breading)
		m_pspkr->Stop();
	else {
		if (g_opt.IsThereAnyVoice())
			m_pspkr->SetEng(g_opt.CurVoice());
		m_pspkr->Speak();
	}
	SetCmdEnabled("aPlay", breading);
	SetCmdEnabled("aStop", !breading);
	SetCmdEnabled("aPrev", true);
	SetCmdEnabled("aNext", true);
}

void CMainWindow::OnChangeSettings(bool)
{
	QMetaObject::Connection cn;
	m_pDlg = new CDlgEngVoice(m_ptts, this);
	cn = connect(m_pDlg, &CDlgEngVoice::SettingsChanged, this, &CMainWindow::OnSettingsChanged);
	ASSERT((bool) cn == true);
	m_pDlg->open();
//	QApplication::restoreOverrideCursor();
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CMainWindow::ReinitTts если tts имеет место быть, удалит его. Создаст новый tts. Если
/// он был, восстановит движок и голос. Настолько варварский метод потому что иначе никак, сука
/// нельзя осуществить навигацыю по тексту - переход к указанной строке: из RHVoice не удаляется
/// текущее задание, оно продолжается после остановки QTextToSpeech::stop(), приходится наглухо
/// убивать QTextToSpeech и создавать вместо него новый с нуля.
///
void CMainWindow::ReinitTts()
{
	QString qsEng;
	QVoice qvc;
	QLocale qlcl;
	COptions::CVoice vc;
	QMetaObject::Connection cn;
	double fVol = .0, fPitch = .0, fRate =.0;
	bool breinit = m_ptts != nullptr;
	if (breinit) {
		qsEng = m_ptts->engine();
		qlcl = m_ptts->locale();
		qvc = m_ptts->voice();
		fVol = m_ptts->volume();	// это не фацает, возвращает нуль, несмотря на то что вносил изменения
		fPitch = m_ptts->pitch();
		fRate = m_ptts->rate();
		delete m_ptts;
	}
	m_ptts = new QTextToSpeech(this);
	if (breinit) {
		if (m_ptts->engine() != qsEng)
			m_ptts->setEngine(qsEng);
		if (m_ptts->locale().name() != qlcl.name())
			m_ptts->setLocale(qlcl);
		if (qvc.name() != m_ptts->voice().name())
			m_ptts->setVoice(qvc);
		m_ptts->setVolume(fVol);// fVol скорее всего нуль. Поэтому в конце ф-ции беру из настроек п-ля.
		m_ptts->setRate(fRate);
		m_ptts->setPitch(fPitch);
	}
	if (m_pspkr) {
		m_pspkr->Stop();
		m_pspkr->Init();
	} else
		m_pspkr = new CTts(&m_ptts, this);
	cn = connect(m_ptts, &QTextToSpeech::sayingWord, m_pspkr, &CTts::OnWord);
	ASSERT((bool) cn == true);
	cn = connect(m_ptts, &QTextToSpeech::stateChanged, m_pspkr, &CTts::OnStateChanged);
	ASSERT((bool) cn == true);
	cn = connect(m_ptts, &QTextToSpeech::errorOccurred, m_pspkr, &CTts::OnError);
	ASSERT((bool) cn == true);
	vc.m_sEng = qPrintable(m_ptts->engine());
	vc.m_sLcl = qPrintable(m_ptts->locale().name());
	vc.m_sVoice = qPrintable(m_ptts->voice().name());
	// почему-то не запоминаются внесённые изменения громкости, тона и темпа. Вернее, их не возвращают
	// соответствующие getters поэтому тут поправочка
	int i = g_opt.FindVoice(vc);
	if (i >= 0 && i < g_opt.m_av.GetCount())
		m_pspkr->SetEng(g_opt.m_av[i]);
}

void CMainWindow::OnReadPrevious(bool)
{	// g_iCurOfst сейчас индексирует следующую строку, которую предстоит читать по завершении этой
	// поэтому шагать нужно не 1, а 2 назад.
	if (int i = (g_iCurOfst > 1) ? g_iCurOfst - 2 : 0) {
		ReinitTts();
		m_pspkr->SetRow((i < g_ao.GetCount()) ? g_ao[i].nRow : 1);
		SetCmdEnabled("aPlay", false);
		SetCmdEnabled("aStop", true);
	}
}

void CMainWindow::OnReadNext(bool)
{	// g_iCurOfst индексирует ещё не прочтанную строку. Двигаюсь к ней.
	if (g_ao.GetCount()) {
		ReinitTts();
		m_pspkr->SetRow(g_ao[g_iCurOfst].nRow);
		SetCmdEnabled("aPlay", false);
		SetCmdEnabled("aStop", true);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CMainWindow::OnSettingsChanged ф-ция привязана к кнопке завершения диалога m_pDlg, так же
/// вызывается в конструкторе, если есть файл для чтения.
/// \param nRow: номер строки в файле
///
void CMainWindow::OnSettingsChanged(int nRow)
{
	if (m_pDlg)
		delete m_pDlg;
	m_pDlg = nullptr;
	if (!g_pf && !OpenFile(g_opt.m_sfp))
		return;
	if (g_iCurOfst > 0) // tts запущен
		ReinitTts();
	m_pspkr->SetRow(nRow);
	SetCmdEnabled("aPlay", false);
	SetCmdEnabled("aStop", true);
	SetCmdEnabled("aPrev", true);
	SetCmdEnabled("aNext", true);
}


void CMainWindow::resizeEvent(QResizeEvent *pe)
{
	QMainWindow::resizeEvent(pe);
	QPlainTextEdit *pte = findChild<QPlainTextEdit*>("plainTextEdit");
	QWidget *pwc = centralWidget();
	pte->setGeometry(pwc->rect());
}