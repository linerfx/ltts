#include "tts.h"
#include <QMainWindow>
#include <QStatusBar>
#include <QPlainTextEdit>
#include "../../common/FxArray.h"
#include "MainWindow.h"
#include "../../common/global.h" // FindNearest<>()

extern glb::CFxArray<COfst, true> g_ao;
extern int32_t g_iCurOfst;
extern long int g_nFileLen;
extern FILE *g_pf;
extern glb::CFxArray<char, true> g_ac;
extern QMainWindow *g_pmw;
namespace fl {
extern int nBlockSz;	// блок диска для ф-цый чтения/записи в CBinFile см. CPath::CreateAppPath
}// namespace fl
extern COptions g_opt;

CTts::CTts(QTextToSpeech **pptts, QObject *pprnt) // pprnt = nullptr
	: QObject{pprnt}
	, m_iRow(0)
	, m_pptts(pptts)
	, m_bStopped(false)
{
	if (!g_ao.GetCount()) {
		g_ao.AddND({ 0, 1 });
		g_iCurOfst = 0;
	} else
		ASSERT(g_iCurOfst >= 0 && g_iCurOfst < g_ao.GetCount());
	ASSERT(fl::nBlockSz > 0);
	// размер буфера меняется тут и в FetchFileString() здесь ниже, больше нигде
	g_ac.SetCountND(1);//fl::nBlockSz);
	g_ac.SetCountND(g_ac.GetObjectsAlloc());
}

void CTts::Init()
{
	m_bStopped = false;
	m_iRow = 0;
}

int ReadFileString();
//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CTts::Speak читаю файл, воспроизвожу звук
///
void CTts::Speak()
{
	if (m_bStopped && m_iRow > 0) {
		m_bStopped = false;
		// m_iRow - индекс строки, а аргумент SetRow() - № строки. Читаю последнюю строку сначала.
		SetRow(m_iRow);// Рекурсия! SetRow() вызывает Speak()
	} else
		OnStateChanged(QTextToSpeech::Ready);
}

void CTts::Stop()
{
	ASSERT(g_iCurOfst > 0);
	QStatusBar *psb = g_pmw->statusBar();
	psb->showMessage("Воспроизведение остановлено");
	(*m_pptts)->stop(QTextToSpeech::BoundaryHint::Utterance);
	m_bStopped = true;// по завершении чтения продолжения не последует
}

void CTts::SetEng(const COptions::CVoice &vc)
{
	ASSERT(m_pptts && *m_pptts);
	QString s = (CCHAR*) vc.m_sEng;
	if ((*m_pptts)->engine() != s)
		(*m_pptts)->setEngine(s);
	s = (CCHAR*) vc.m_sLcl;
	if ((*m_pptts)->locale().name() != s) {
		QList<QLocale> al = (*m_pptts)->availableLocales();
		for (int i = 0, n = al.count(); i < n; i++)
			if (al[i].name() == s) {
				(*m_pptts)->setLocale(al[i]);
				break;
			}
	}
	s = (CCHAR*) vc.m_sVoice;
	if ((*m_pptts)->voice().name() != s) {
		QList<QVoice> av = (*m_pptts)->availableVoices();
		for (int i = 0, n = av.count(); i < n; i++)
			if (av[i].name() == s) {
				(*m_pptts)->setVoice(av[i]);
				break;
			}
	}
	(*m_pptts)->setVolume(vc.m_fVolume);
	(*m_pptts)->setPitch(vc.m_fPitch);
	(*m_pptts)->setRate(vc.m_fRate);
}

void CTts::SetVc(COptions::CVoice &vc)
{
	vc.m_sEng = qUtf8Printable((*m_pptts)->engine());
	vc.m_sLcl = qUtf8Printable((*m_pptts)->locale().name());
	vc.m_sVoice = qUtf8Printable((*m_pptts)->voice().name());
	vc.m_fVolume = (*m_pptts)->volume();
	vc.m_fPitch = (*m_pptts)->pitch();
	vc.m_fRate = (*m_pptts)->rate();
}

int cmp_row(const COfst &a, const COfst &z)
{
	return (a.nRow < z.nRow) ? -1 : (a.nRow > z.nRow) ? 1 : 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CTts::SetRow ставлю курсор в файле на указанной строке и начинаю её читать
/// \param nRow: № строки, не индекс (нуль - не действительный №)
/// \return тек.строку m_iRow
///
int CTts::SetRow(int nRow)
{
	if (nRow <= 0)
		return -1; // не действительный № не рассматривается
	COfst ofst = { 0, nRow };
	// т.к. все значения в g_ao естественно уложены в возрастающем порядке, можно применять поиск в упорядоченном массиве
	int i = glb::FindNearest((const COfst*) g_ao, g_ao.GetCount(), ofst, &cmp_row);
	if (i == g_ao.GetCount()) {// где-то дальше в файле
		while (nRow > g_ao[g_iCurOfst].nRow)
			if (!ReadFileString())
				break; // ошибка, или файл весь прочитан
		// сначала о плохом
		if (nRow > g_ao[g_iCurOfst].nRow)
			nRow = g_ao[g_iCurOfst].nRow;// нет такой строки в файле, активной будет последняя известная строка
		// иначе строка найдена
		m_iRow = nRow - 1;
	}// искомая строка находится где-то раньше известного начала (это вряд-ли т.к. при иницыацыи читаю файл сначала)
	else if (i == 0 && nRow < g_ao[0].nRow) {
		COfst &o = g_ao[0];
		o.nRow = nRow;
		o.nOfst = 0;
		m_iRow = 0;
		g_iCurOfst = -1;// придётся начать с начала файла
		g_ao.SetCountND(1);
	} else { // указанная строка найдена в массиве; возвращаюсь к ней
		ASSERT(nRow == g_ao[i].nRow);
		m_iRow = nRow - 1;
		g_iCurOfst = i;
		g_ao.SetCountND(i + 1);
	}
	Speak();
	return m_iRow;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CTts::OnWord связывается в main() с ф-цией QTextToSpeech::sayingWord()
/// \param word: прочитанное слово
/// \param id: х.з.
/// \param start:
/// \param length
///
void CTts::OnWord(const QString &word, qsizetype id, qsizetype start, qsizetype length)
{
//	не фацает с RHVoice, нет такой возможности. Только пуск и стоп, без паузы и продолжения.
}

void CTts::OnError(QTextToSpeech::ErrorReason reason, const QString &errorString)
{
	QStatusBar *psb = g_pmw->statusBar();
	psb->showMessage(QString("ошибка ttl: %2").arg(errorString));
	if (g_pf) {
		fclose(g_pf);
		g_pf = NULL;
		exit(0);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CTts::OnStateChanged здесь ловлю Ready, оно отправляется по завершении воспроизведения строки. Здесь
/// его ловлю и продолжаю читать файл.
/// \param state
///
void CTts::OnStateChanged(QTextToSpeech::State state)
{
	ASSERT(!g_ao.GetCount() && g_iCurOfst < 0 || g_iCurOfst >= 0 && g_iCurOfst < g_ao.GetCount());
	int n = 0;
	COfst ofst;
	QStatusBar *psb = nullptr;
	switch (state) {
	case QTextToSpeech::Ready:
		if (m_bStopped)
			break;
		if (g_ao.GetCount())
			ofst = g_ao[g_iCurOfst];
		while (m_iRow < ofst.nRow) {
			if (!(n = ReadFileString()))
				break;
			m_iRow++;
		}// здесь индекс m_iRow и g_iCurOfst равны и указывают на следующий (последний) отступ в g_ao, т.е.
		// на следующую за g_ac строку. А g_ac содержит текст прочитанной, ещё не воспроизведённой строки.
		if (n > 0) {
			(*m_pptts)->say(QByteArray::fromRawData(g_ac, n));
			QPlainTextEdit *pte = g_pmw->findChild<QPlainTextEdit*>("plainTextEdit");
			ASSERT(pte);
			pte->appendPlainText(QString("%1. %2").arg(m_iRow).arg((CCHAR*) g_ac));
			pte->moveCursor(QTextCursor::End);
			pte->ensureCursorVisible();
		} else {// ошибка или конец файла
			fclose(g_pf);
			g_pf = NULL;
			CMainWindow *pmw = reinterpret_cast<CMainWindow*>(g_pmw);
			pmw->SetCmdEnabled("aPlay", true);
			pmw->SetCmdEnabled("aStop", false);
			pmw->SetCmdEnabled("aPrev", true);
			pmw->SetCmdEnabled("aNext", true);
		}
		break;
	case QTextToSpeech::Speaking:
		ASSERT(g_iCurOfst > 0);
		psb = g_pmw->statusBar();
		psb->showMessage(QString("Читаю абзац: %1 (~%2%)").arg((int) g_ao[g_iCurOfst - 1].nRow).
						 arg((double) g_ao[g_iCurOfst - 1].nOfst / (double) g_nFileLen * 100.0, 0, 'f', 1));
		break;
	case QTextToSpeech::Paused:
		ASSERT(g_iCurOfst > 0);
		psb = g_pmw->statusBar();
		psb->showMessage("На паузе...");
		break;
	case QTextToSpeech::Error:
		ASSERT(g_iCurOfst > 0);
		psb = g_pmw->statusBar();
		psb->showMessage(QString("Ошибка: %1").arg((*m_pptts)->errorString()));
		break;
	case QTextToSpeech::Synthesizing:
		ASSERT(g_iCurOfst > 0);
		psb = g_pmw->statusBar();
		psb->showMessage("Синтезирую текст");
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief TrimString убираю пробелы сначала и в конце g_ac
/// \return к-во символов в строке
///
int TrimString()
{
	int i, n = strlen(g_ac);
	for (i = n - 1; i >= 0; i--)
		if (isspace(g_ac[i])) {
			g_ac[i] = '\0';
			n--;
		} else
			break;
	for (i = 0; i < n; i++)
		if (!isspace(g_ac[i]))
			break;
	if (i > 0 && n - i > 1)
		memcpy(g_ac, g_ac + i, n - i + 1);// вместе с завершающим нулём
	if ((n -= i) <= 0) {
		g_ac[0] = '\0';
		return 0;
	}
	return n;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief FetchFileString читаю одну строку из файла с тек.позиции. Задачей является извлечение
/// всей строки из файла, какой длины она бы ни была, чтобы не склеивать Utf-8 символы выше.
/// \return внутренний ук-ль g_ac или нуль, если файл пройден до конца, или возникла ошибка
///
const char* FetchFileString()
{	// если в буфер не вмещается строка, последний символ в буфере будет скорее всего нулём, т.к.
	// fges() должна завершать строку нулём. Либо какой-то символ или часть символа Utf-8.
	ASSERT(g_ac.GetCount() == g_ac.GetObjectsAlloc());
	const char *pc;
	int32_t i = 0;
	g_ac.Last() = 0x7F; // https://cplusplus.com/reference/cctype/
	while ((pc = fgets(g_ac + i, g_ac.GetCount() - i, g_pf)) != nullptr && g_ac.Last() != 0x7F) {
		i = (g_ac.Last() == '\0') ? g_ac.GetCount() - 1 : g_ac.GetCount();
		g_ac.SetCountND(g_ac.GetObjectsAlloc() + 1);
		g_ac.SetCountND(g_ac.GetObjectsAlloc());
		g_ac.Last() = 0x7F;
	}
	return pc;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief ReadFileString читаю строку из файла с известного места
/// \return к-во символов в строке
///
int ReadFileString()
{
	ASSERT(!g_ao.GetCount() && g_iCurOfst < 0 || g_iCurOfst >= 0 && g_iCurOfst < g_ao.GetCount());
	ASSERT(g_pf);
	const char *pc = nullptr;
	int n = 0;
	if (feof(g_pf))
		return 0;
	// после иницыацыи
	if (g_iCurOfst < 0)
		g_iCurOfst = g_ao.AddND({ 0, 1 });
	// определюсь с файловым отступом
	COfst ofst = g_ao[g_iCurOfst];
	if (ftell(g_pf) != ofst.nOfst)
		fseek(g_pf, ofst.nOfst, SEEK_SET);
	// получу не пустую строку, пропущу пустые
	while ((pc = FetchFileString()) != nullptr)
		if ((n = TrimString()) > 0)
			break;
	if (pc == nullptr) {
		QStatusBar *psb = g_pmw->statusBar();
		psb->showMessage(QString("ошибка чтения файла: %1").arg(strerror(errno)));
		return -1;
	} // else отступ теперь такой
	g_iCurOfst = g_ao.AddND({ ftell(g_pf), ofst.nRow + 1 });
	return n;
}

