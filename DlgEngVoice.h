#ifndef DLGENGVOICE_H
#define DLGENGVOICE_H

#include <QDialog>
#include <QTextToSpeech>
#include "Options.h"

namespace Ui {
class CDlgEngVoice;
}

class CDlgEngVoice : public QDialog
{
	Q_OBJECT
	QTextToSpeech *m_ptts;
	COptions::CVoice m_vc;
	int32_t m_nRow;// номер строки в файле

public:
	explicit CDlgEngVoice(QTextToSpeech *ptts, QWidget *parent = nullptr);
	~CDlgEngVoice();

	int SetEngines();
	int SetLocales();
	int SetVoices();
	void SetAbility();

	const COptions::CVoice& Voice() const;

public slots:
	void OnEngChanged(int);
	void OnVoiceChanged(int);	// не фацает QComboBox::currentIndexChanged()
	void OnLclChanged(int);
	void OnRowChanged(const QString &s);
	void OnAccepted();
	void OnVolumeChanged(int);
	void OnPitchChanged(int);
	void OnRateChanged(int);

signals:
	void SettingsChanged(int iRow);

private:
	Ui::CDlgEngVoice *ui;
};

inline const COptions::CVoice &CDlgEngVoice::Voice() const
{
	return m_vc;
}

#endif // DLGENGVOICE_H
