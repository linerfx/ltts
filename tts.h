#ifndef CTTS_H
#define CTTS_H
#define CBUF (1024 * 8)

#include <QObject>
#include <QTextToSpeech>
#include "Options.h"

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CTts class читаю файл, вывожу текст в представление гл.окна
///
class CTts : public QObject
{
	Q_OBJECT

	QTextToSpeech **m_pptts;
	int m_iRow; // индекс файловой строки, подлежащей чтению
	bool m_bStopped;

public:
	CTts(QTextToSpeech **pptts, QObject *parent = nullptr);
	void Init();
	void Speak();
	void Stop();
	int SetRow(int iRow);
	bool Stopped() const;
	void ClearStopState();

	void SetEng(const COptions::CVoice &vc);	// m_vc to QTextToSpeech
	void SetVc(COptions::CVoice &vc);			// QTextToSpeech to m_vc

public slots:
	void OnWord(const QString &word, qsizetype id, qsizetype start, qsizetype length);
	void OnError(QTextToSpeech::ErrorReason reason, const QString &errorString);
	void OnStateChanged(QTextToSpeech::State state);

signals:
};

inline bool CTts::Stopped() const
{
	return m_bStopped;
}

inline void CTts::ClearStopState()
{
	m_bStopped = false;
}

#endif // CTTS_H
