#include <csignal>
#include <cstdarg>
#include "../../common/defs.h"
#include "../../common/exception.h"
#include "../../common/ErrLog.h"
#include "../../common/io_path.h"
#include "Options.h"
#include "MainWindow.h"
#include <QStandardPaths>

namespace glb {
extern thread_local CFxString sAppPath;
}
namespace err {
extern CLog log;
}
extern glb::CFxArray<COfst, true> g_ao;// файловые отступы для быстрого перехода по пройденным параграфам
extern int32_t g_iCurOfst;	// индекс тек.эл-та в g_ao

COptions g_opt;

///////////////////////////////////////////////////////////////////////////////
/// \brief OnInit иницыацыя: читаю сохранённые настройки п-ля с диска на старте приложения.
/// Вызывается в main()
/// \return трю/хрю
///
bool OnInit()
{
	QStringList as = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation);
	QByteArray ab = as.first().toUtf8();
	ASSERT(ab.length() == strlen(ab));
	glb::CFxString s(ab, ab.length()); // сука ab.length() считает завершающий нуль
	fl::CPath::CheckMakeDir(fl::CPath::AddName(s, "ltts"));
	errno = 0;
	FILE *pf = fopen(fl::CPath::AddName(s, "opts.bin"), "rb");
	if (!pf) {
		int e = errno;
		if (e == ENOENT)
			return false;// нет файла, допускается. Настройки остаются по-умолчанию
		throw err::CCException(e, "%t Ошибка при открытии файла настроек (%d): %s.", time(NULL), e, strerror(e));
	}
	try {
		g_opt = pf;
	} catch (...) {
		fclose(pf);
		throw;
	}
	fclose(pf);
	return true;
}

void AtExit()
{
	FILE *pf = NULL;
	try {
		if (g_iCurOfst > 0) // указывает на следующий отступ
			g_opt.m_nRow = g_ao[g_iCurOfst - 1].nRow;
		if ((pf = g_opt.OpenFile()) != NULL)
			g_opt.PutFile(pf); // закроет файл по завершении записи
	} catch (...) {
		err::log.Add("%t исключение при завершении программы", time(NULL));
	}
}

void on_signal(int n)
{
	switch (n) {
//	case SIGRTMIN:
//		break;
	case SIGABRT:	// Abnormal termination
		break;
	case SIGINT:	// Interactive attention signal
		break;
	case SIGILL:	// Illegal instruction
		break;
	case SIGFPE:	// Erroneous arithmetic operation
		break;
	case SIGSEGV:	// Invalid access to storage
		break;
	case SIGTERM:	// Termination request
		break;
	// Historical signals specified by POSIX
	case SIGHUP:	// Hangup
		break;
	case SIGQUIT:	// Quit
		break;
	case SIGTRAP:	// Trace/breakpoint trap
		break;
	case SIGKILL:	// Killed
		break;
	case SIGBUS:	// Bus error
		break;
	case SIGSYS:	// Bad system call
		break;
	case SIGPIPE:	// Broken pipe
		break;
	case SIGALRM:	// Alarm clock
		break;
	// New(er) POSIX signals (1003.1-2008, 1003.1-2013)
	case SIGURG:	// Urgent data is available at a socket
		break;
	case SIGSTOP:	// Stop, unblockable
		break;
	case SIGTSTP:	// Keyboard stop
		break;
	}
}

namespace err {
CLog log;

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLocalException::CLocalException конструктор местного исключения здесь потому что
/// все приложения, которые используют это исключение по-разному хранят текстовые строки с
/// описанием ошибки. iTog хранит строки в файловом массиве, iTgClnt получает их от iTog по
/// сети на старте, здесь пока нет никакого хранилища, возможно буду использовать Qt-переводчик
/// \param nStrErrorID: ид-р ошибки из common/strdef.h
///
CLocalException::CLocalException(int nStrErrorID, ...)
{
	va_list vlist;
	va_start(vlist, nStrErrorID);
	m_er = nStrErrorID;
	try {
		log.Add(FrmtStr(m_er), vlist);
	} catch (...) {
		va_end(vlist);
		throw;
	}
	va_end(vlist);
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLocalException::FrmtStr статич. ф-ция: строки формата сообщений ошибок беру с
/// сервера на старте.
/// \param iStr: индекс SI_*** из common/strdef.h
/// \return
///
const char* CLocalException::FrmtStr(int32_t iStr)
{
	CCHAR *ps = nullptr;
	switch (iStr) {
	case SI_NED:
		ps = "%t недостаточно сведений '%s':%d.";
		break;
	case SI_CE:
		ps = "%t С-исключение '%s' (%d) в файле \'%s\':%d.";
		break;
	default:
		ASSERT(false);
	}
	return ps;
}

const char* CLog::PathName(glb::CFxString &s) const
{
	const int cfn = 32;
	char pszfn[cfn];
	const char *pcszPath = glb::sAppPath;
	if (!pcszPath)
		return nullptr;
	s = pcszPath;
	time_t ntm = time(NULL);
	struct tm *pstm = localtime(&ntm);
	pstm->tm_yday -= !(pstm->tm_yday % 2);		// один файл содержит записи за 2 дня
	strftime(pszfn, cfn, "lg-%Y-%j.txt", pstm);	// год день года lg-YYYY-jjj.txt
	return (!m_sDir.GetCount() ? fl::CPath::AddName(s, pszfn) :
				fl::CPath::AddName(fl::CPath::AddName(s, m_sDir), pszfn));
}


}// nasepace err