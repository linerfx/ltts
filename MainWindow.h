#ifndef CMAINWINDOW_H
#define CMAINWINDOW_H

#include <QMainWindow>
#include <QTextToSpeech>
#include "tts.h"
#include "DlgEngVoice.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class CMainWindow;
}
QT_END_NAMESPACE

class CMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	QTextToSpeech *m_ptts;
	CTts *m_pspkr;
	CDlgEngVoice *m_pDlg;

public:
	CMainWindow(QWidget *parent = nullptr);
	~CMainWindow();
	bool OpenFile(const char *pcszPn = nullptr);
	QTextToSpeech& Tts();
	void SetCmdEnabled(const char *cmdName, bool bEnable);

private slots:
	void OnOpenFile(bool);
	void OnPlayOrStop(bool);
	void OnChangeSettings(bool);
	void ReinitTts();
	void OnReadPrevious(bool);
	void OnReadNext(bool);
	void OnSettingsChanged(int nRow);

private:
	Ui::CMainWindow *ui;

	// QWidget interface
protected:
	virtual void resizeEvent(QResizeEvent *event);
};

struct COfst {
	int64_t nOfst = 0;	// файловый отступ
	int32_t nRow = 1;	// № параграфа/строки из файла, не индекс
};

#endif // CMAINWINDOW_H
