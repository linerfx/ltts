#include "Options.h"
#include <sys/stat.h>
#include <QStandardPaths>
#include <QStringList>
#include "../../common/io_path.h"

COptions::COptions()
	: m_iCur(-1)
	, m_nRow(0)	// порядковый № строки, аналогичный COfst::nRow
{

}

////////////////////////////////////////////////////////////////////////////////////
/// \brief COptions::operator =() читаю настройки из файла
/// \param pf: открытый файл
/// \return этот объект
///
const COptions& COptions::operator =(FILE *pf)
{
	ASSERT(pf);
	int i = 0, e;
	size_t n = 0;
	const int cbit = 128;
	glb::CFxArray<BYTE, true> ab(cbit);
	ab.SetCount(1);
	ab.SetCountND(ab.GetObjectsAlloc());
	errno = 0;
	while (!feof(pf)) {
		i += (int) n;
		n = fread(ab + i, sizeof(BYTE), ab.GetCount() - i, pf);
		if ((e = errno) != 0)
			throw err::CCException(e, "%t Ошибка чтения файла настроек (%d): %s.", time(nullptr), e, strerror(e));
		if (n < cbit)
			break;// можно continue т.к. это скорее всего eof
		ab.SetCount(1 + ab.GetCount());
		ab.SetCountND(ab.GetObjectsAlloc());
	}
	if (n != cbit)
		ab.SetCountND(ab.GetCount() - cbit + n);
	if ((n = ab.GetCount()) > 0)
		SetBinData(ab, n);
	return *this;
}

FILE* COptions::OpenFile()
{
	QStringList as = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation);
	QByteArray ab = as.first().toUtf8();
	glb::CFxString s(ab, ab.length()); // сука ab.length() уже включает завершающий нуль
	fl::CPath::CheckMakeDir(fl::CPath::AddName(s, "ltts"));
	errno = 0;
	FILE *pf = fopen(fl::CPath::AddName(s, "opts.bin"), "wb");
	if (!pf) {
		int e = errno;
		throw err::CCException(e, "%t Ошибка (%d) при открытии файла %s: %s.", time(NULL), e, (CCHAR*) s, strerror(e));
	}
	return pf;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief COptions::PutFile записываю настройки в файл
/// \param pf: файл настроек при открытии всегда освобождается
/// \param bClose: закрыть файл по завершении записи
/// \return к-во записанный байт
///
int32_t COptions::PutFile(FILE *pf, bool bClose) // bClose = true
{
	glb::CFxArray<BYTE, true> ab(128);
	GetBinData(ab);
	errno = 0;
	if (fwrite(ab, sizeof(BYTE), ab.GetCount(), pf) != sizeof(BYTE) * ab.GetCount()) {
		int e = errno;
		fclose(pf);
		throw err::CCException(e, "%t Ошибка записи в файл настроек (%d): %s.", time(NULL), e, strerror(e));
	}
	if (bClose)
		fclose(pf);
	return ab.GetCount();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief COptions::SetUpdate добавляет "голос" в мссив, упорядочивая его по возрастанию
/// \param v: новый "голос"
/// \return индекс нового "голоса" в массиве
///
int32_t COptions::SetUpdate(const CVoice &v)
{
	int i = 0, c = 0;
	while (i < m_av.GetCount() && (c = m_av[i].cmp(v)) < 0)
		i++;
	if (i == m_av.GetCount())
		m_av.AddND(v);
	else if (c == 0)
		m_av[i] = v;
	else
		m_av.Insert(i, v);
	return i;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief COptions::CVoice::GetBinData записываю содержимое объекта в массив байт
/// \param ab: массив байт
/// \return к-во записанных байт
///
int32_t COptions::CVoice::GetBinData(BYTES &ab)
{
	cint32_t c = ab.GetCount();
	m_sEng.GetBinData(ab);
	m_sLcl.GetBinData(ab);
	m_sVoice.GetBinData(ab);
	ab.AddND((CBYTE*) &m_fRate, sizeof(double) * 3);
	return (ab.GetCount() - c);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief COptions::CVoice::SetBinData читаю из массива байт содержимое объекта, ранеезаписанное
/// туда ф-цие GetBinData()
/// \param pb: массив байт
/// \param cnt: к-во байт в массиве
/// \return к-во прочитанных байт
///
int32_t COptions::CVoice::SetBinData(const BYTE *cpb, cint32_t cnt)
{
	int32_t i = m_sEng.SetBinData(cpb, cnt);
	i += m_sLcl.SetBinData(cpb + i, cnt - i);
	i += m_sVoice.SetBinData(cpb + i, cnt - i);
	return (i + glb::read_raw((BYTE*) &m_fRate, sizeof(double) * 3, cpb + i, cnt- i));
}
