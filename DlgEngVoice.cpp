#include "DlgEngVoice.h"
#include "./ui_DlgEngVoice.h"
#include <QComboBox>
#include <QStatusBar>
#include "MainWindow.h"
#include "../../common/FxArray.h"

extern glb::CFxArray<COfst, true> g_ao;
extern int32_t g_iCurOfst;
extern COptions g_opt;

CDlgEngVoice::CDlgEngVoice(QTextToSpeech *ptts, QWidget *parent)
	: QDialog(parent)
	, ui(new Ui::CDlgEngVoice)
	, m_ptts(ptts)
	, m_nRow(-1)
{
	QMetaObject::Connection cn;
	int i, n;
	ui->setupUi(this);
	SetEngines();
	SetLocales();
	SetVoices();
	QComboBox *pcb = findChild<QComboBox*>("cbEng");
	cn = connect(pcb, &QComboBox::currentIndexChanged, this, &CDlgEngVoice::OnEngChanged);
	ASSERT((bool) cn == true);
	pcb = findChild<QComboBox*>("cbLocale");
	cn = connect(pcb, &QComboBox::currentIndexChanged, this, &CDlgEngVoice::OnLclChanged);
	ASSERT((bool) cn == true);
	pcb = findChild<QComboBox*>("cbVoice");
	cn = connect(pcb, &QComboBox::currentIndexChanged, this, &CDlgEngVoice::OnVoiceChanged);
	ASSERT((bool) cn == true);
	QLineEdit *ple = findChild<QLineEdit*>("leRow");
	ple->setText(QString::number(m_nRow = glb::Mx(1, g_ao[g_iCurOfst].nRow - 1)));
	cn = connect(ple, &QLineEdit::textChanged, this, &CDlgEngVoice::OnRowChanged);
	ASSERT((bool) cn == true);
	QDialogButtonBox *pbb = findChild<QDialogButtonBox*>("buttonBox");
	if (pbb) {
		cn = connect(pbb, &QDialogButtonBox::accepted, this, &CDlgEngVoice::OnAccepted);
		ASSERT((bool) cn == true);
		cn = connect(pbb, &QDialogButtonBox::rejected, this, [this]{ emit SettingsChanged(-1); });
		ASSERT((bool) cn == true);
	}// непосредственно "голос" в настройках
	m_vc.m_sEng = qUtf8Printable(ptts->engine());
	m_vc.m_sLcl = qUtf8Printable(ptts->locale().name());
	m_vc.m_sVoice = qUtf8Printable(ptts->voice().name());
	m_vc.m_fVolume = ptts->volume();
	m_vc.m_fRate = ptts->rate();
	m_vc.m_fPitch = ptts->pitch();
	COptions::CVoice *pv;// это потому что QTextToSpeech не хранит уровни: громкость, высоту и темп.
	if ((i = g_opt.FindVoice(m_vc)) >= 0 && i < g_opt.m_av.GetCount()) {
		m_vc = g_opt.m_av[i];
		g_opt.m_iCur = i;
	}// громкость
	QSlider *ps = findChild<QSlider*>("slVolume");
	cn = connect(ps, &QSlider::valueChanged, this, &CDlgEngVoice::OnVolumeChanged);
	ASSERT((bool) cn == true);
	ps->setRange(0, 100);
	ps->setSliderPosition((int) (m_vc.m_fVolume * 100.0));
	ps->setTickInterval(100);
	ps->setTickPosition(QSlider::TicksAbove);
	// высота звука
	ps = findChild<QSlider*>("slPitch");
	cn = connect(ps, &QSlider::valueChanged, this, &CDlgEngVoice::OnPitchChanged);
	ASSERT((bool) cn == true);
	ps->setRange(0, 100);
	ps->setSliderPosition((int) ((1.0 + m_vc.m_fPitch) * 100.0 / 2.0));
	ps->setTickInterval(50);
	ps->setTickPosition(QSlider::TicksAbove);
	// темп/скорость воспроизведения
	ps = findChild<QSlider*>("slRate");
	cn = connect(ps, &QSlider::valueChanged, this, &CDlgEngVoice::OnRateChanged);
	ASSERT((bool) cn == true);
	ps->setRange(0, 100);
	ps->setSliderPosition((int) ((1.0 + m_vc.m_fRate) * 100.0 / 2.0));
	ps->setTickInterval(50);
	ps->setTickPosition(QSlider::TicksAbove);
	// возможности движка
	SetAbility();
	findChild<QCheckBox*>("chbCanSpeak")->setEnabled(false);
	findChild<QCheckBox*>("chbWord")->setEnabled(false);
	findChild<QCheckBox*>("chbPauseRes")->setEnabled(false);
	findChild<QCheckBox*>("chbSynthesize")->setEnabled(false);
}

CDlgEngVoice::~CDlgEngVoice()
{
	delete ui;
}

int CDlgEngVoice::SetEngines()
{
	QStringList as = m_ptts->availableEngines();
	QString s = m_ptts->engine();
	QComboBox *pcb = findChild<QComboBox*>("cbEng");
	int i, n;
	ASSERT(pcb);
	pcb->clear();
	for (i = 0, n = as.count(); i < n; i++) {
		pcb->addItem(as[i]);
		if (as[i] == s)
			pcb->setCurrentIndex(i);
	}
	return n;
}

int CDlgEngVoice::SetLocales()
{
	QList<QLocale> al = m_ptts->availableLocales();
	QLocale *pl, l = m_ptts->locale();
	QComboBox *pcb = findChild<QComboBox*>("cbLocale");
	int i, n;
	ASSERT(pcb);
	pcb->clear();
	for (i = 0, n = al.count(); i < n; i++) {
		pl = &al[i];
		pcb->addItem(pl->nativeTerritoryName() + ", " + pl->nativeLanguageName(), pl->name());
		if (pl->name() == l.name())
			pcb->setCurrentIndex(i);
	}
	return n;
}

int CDlgEngVoice::SetVoices()
{
	QList<QVoice> av = m_ptts->availableVoices();
	QVoice v = m_ptts->voice();
	QComboBox *pcb = findChild<QComboBox*>("cbVoice");
	int i, n;
	pcb->clear();
	for (i = 0, n = av.count(); i < n; i++) {
		pcb->addItem(av[i].name());
		if (av[i].name() == v.name())
			pcb->setCurrentIndex(i);
	}
	return n;
}

void CDlgEngVoice::SetAbility()
{
	QTextToSpeech::Capabilities c = m_ptts->engineCapabilities();
	QCheckBox *pcb = findChild<QCheckBox*>("chbCanSpeak");
	pcb->setCheckState((c & QTextToSpeech::Capability::Speak) ? Qt::Checked : Qt::Unchecked);
	pcb = findChild<QCheckBox*>("chbWord");
	pcb->setCheckState((c & QTextToSpeech::Capability::WordByWordProgress) ? Qt::Checked : Qt::Unchecked);
	pcb = findChild<QCheckBox*>("chbPauseRes");
	pcb->setCheckState((c & QTextToSpeech::Capability::PauseResume) ? Qt::Checked : Qt::Unchecked);
	pcb = findChild<QCheckBox*>("chbSynthesize");
	pcb->setCheckState((c & QTextToSpeech::Capability::Synthesize) ? Qt::Checked : Qt::Unchecked);
}

void CDlgEngVoice::OnEngChanged(int)
{
	QComboBox *pcb = qobject_cast<QComboBox*>(sender());
	QString s = pcb->currentText();
	if (s.length() && m_ptts->engine() != s) {
		m_vc.m_sEng = qUtf8Printable(s);
		if (m_ptts->setEngine(s)) {// меняю места и голоса
			SetLocales();
			SetVoices();
			SetAbility();
		} else
			qDebug() << "не получается изменить движок";
	} else
		m_vc.m_sEng = qUtf8Printable(s);
}

void CDlgEngVoice::OnVoiceChanged(int)
{
	QComboBox *pcb = qobject_cast<QComboBox*>(sender());
	QString s = pcb->currentText();
	if (s.length() && m_ptts->voice().name() != s) {
		QList<QVoice> av = m_ptts->availableVoices();
		for (int i = 0, n = av.count(); i < n; i++)
			if (av[i].name() == s) {
				m_ptts->setVoice(av[i]);
				m_vc.m_sVoice = qUtf8Printable(s);
				if ((i = g_opt.FindVoice(m_vc)) >= 0) {
					g_opt.m_iCur = i;
					m_vc = g_opt.m_av[i];
					QSlider *ps;
					ps = findChild<QSlider*>("slVolume");
					ps->setSliderPosition((int) (m_vc.m_fVolume * 100.0));
					ps = findChild<QSlider*>("slRate");
					ps->setSliderPosition((int) ((1.0 + m_vc.m_fRate) * 100.0 / 2.0));
					ps = findChild<QSlider*>("slPitch");
					ps->setSliderPosition((int) ((1.0 + m_vc.m_fPitch) * 100.0 / 2.0));
					SetAbility();
				}
				break;
			}
	} else // голос в движке может отличаться если изменился движок или место
		m_vc.m_sVoice = qUtf8Printable(s);
}

void CDlgEngVoice::OnLclChanged(int)
{
	QComboBox *pcb = qobject_cast<QComboBox*>(sender());
	QString s = pcb->currentData().toString();
	if (s.length() && m_ptts->locale().name() != s) {
		QList<QLocale> al = m_ptts->availableLocales();
		for (int i = 0, n = al.count(); i < n; i++)
			if (al[i].name() == s) {
				m_ptts->setLocale(al[i]);
				m_vc.m_sLcl = qUtf8Printable(s);
				SetVoices();
				SetAbility();
				if ((i = g_opt.FindVoice(m_vc)) >= 0) {
					g_opt.m_iCur = i;
					m_vc = g_opt.m_av[i];
					QSlider *ps;
					ps = findChild<QSlider*>("slVolume");
					ps->setSliderPosition((int) (m_vc.m_fVolume * 100.0));
					ps = findChild<QSlider*>("slRate");
					ps->setSliderPosition((int) ((1.0 + m_vc.m_fRate) * 100.0 / 2.0));
					ps = findChild<QSlider*>("slPitch");
					ps->setSliderPosition((int) ((1.0 + m_vc.m_fPitch) * 100.0 / 2.0));
				}
				break;
			}
	} else// место в движке может отличаться, если изменился движок
		m_vc.m_sLcl = qUtf8Printable(s);
}

void CDlgEngVoice::OnRowChanged(const QString &s)
{
	m_nRow = s.toInt();
}

void CDlgEngVoice::OnAccepted()
{
	FILE *pf = nullptr;
	g_opt.m_iCur = g_opt.SetUpdate(m_vc);
	if ((pf = g_opt.OpenFile()) != nullptr)
		g_opt.PutFile(pf); // закроет файл по завершении записи
	emit SettingsChanged(m_nRow);
}

void CDlgEngVoice::OnVolumeChanged(int n)
{
	m_vc.m_fVolume = n / 100.0;
	m_ptts->setVolume(m_vc.m_fVolume);
}

void CDlgEngVoice::OnPitchChanged(int n)
{
	m_vc.m_fPitch = n / 100.0 * 2.0 - 1.0;
	m_ptts->setPitch(m_vc.m_fPitch);
}

void CDlgEngVoice::OnRateChanged(int n)
{
	m_vc.m_fRate = n / 100.0 * 2.0 - 1.0;
	m_ptts->setRate(m_vc.m_fRate);
}
