#ifndef COPTIONS_H
#define COPTIONS_H
#include "../../common/FxString.h"
#include "../../common/FxVArray.h"

class COptions
{
public:
	struct CVoice
	{
		glb::CFxString
			m_sEng,		// движок
			m_sLcl,		// место
			m_sVoice;	// голос
		double m_fRate,	// темп -1.0 to 1.0
			m_fVolume,	// громкость 0.0 to 1.0
			m_fPitch;	// высота -1.0 to 1.0
		CVoice();
		CVoice(const CVoice &o);
		CVoice(CVoice &&o);
		const CVoice& operator =(const CVoice &o);
		CVoice& operator =(CVoice &&o);
		int32_t GetBinData(BYTES &ab);
		int32_t SetBinData(const BYTE *cpb, cint32_t cnt);
		int cmp(const CVoice &v) const;
		static int scmp(const CVoice &a, const CVoice &z);
		bool IsEmpty() const;
	};
	glb::CvArray<CVoice> m_av;
	glb::CFxString m_sfp;// путь к файлу
	int m_iCur, // индекс активный "голоса" из QTts::availableVoices()
		m_nRow;	// № последней читаемой строки

public:
	COptions();

public:
	// файловые операцыи
	const COptions& operator =(FILE *pf);
	FILE* OpenFile();
	int32_t PutFile(FILE *pf, bool bClose = true);
	// запись/извлечение массива байт
	int32_t SetBinData(CBYTE *cpb, int32_t cnt);
	int32_t GetBinData(glb::CFxArray<BYTE, true> &ab);
	// добавляю или обновляю "голос"
	int32_t SetUpdate(const CVoice &v);
	//
	const CVoice& CurVoice() const;
	int32_t FindVoice(const CVoice &v) const;
	bool IsThereAnyVoice() const;
	bool IsThereAFiletoRead() const;

	// Сведения для отображения:
	//
	// Движок, изменяемые параметры:
	// rate - темп
	// volume - громкость
	// pitch - высота
	//
	// возможности движка, только для чтения:
	// QTextToSpeech::Capability::Speak
	// QTextToSpeech::Capability::PauseResume
	// QTextToSpeech::Capability::WordByWordProgress
	// QTextToSpeech::Capability::Synthesize
	//
	// Нужно заметить, на QTextToSpeech::evailableLanquages() влияет локализацыя, нужно сначала запросить
	// имеющиеся для движка локализацыи и после выбирать голос.
	//
	// голос (всё только для чтения):
	// возраст
	// пол
	// язык
	// имя
	// регион (локализацыя)
	//
	// из локализацыи QLocale можно дохуя что получить, например:
	// язык: QLocale::languageToString(QLocale::lanquage())
	//		QLocale::nativeLanguageName()
	// страна/территория: QLocale::territoryToString(QLocale::territory())
	//		QLocale::nativeTerritoryName()
	// шрифт (начертание?): QLocale::scriptToString(QLocale::script())
};

inline COptions::CVoice::CVoice()
	: m_fRate(.0)
	, m_fVolume(.0)
	, m_fPitch(.0)
{

}

inline COptions::CVoice::CVoice(const CVoice &o)
	: m_sEng(o.m_sEng)
	, m_sLcl(o.m_sLcl)
	, m_sVoice(o.m_sVoice)
	, m_fRate(o.m_fRate)
	, m_fVolume(o.m_fVolume)
	, m_fPitch(o.m_fPitch)
{

}

inline COptions::CVoice::CVoice(CVoice &&o)
	: m_sEng(o.m_sEng)
	, m_sLcl(o.m_sLcl)
	, m_sVoice(o.m_sVoice)
	, m_fRate(o.m_fRate)
	, m_fVolume(o.m_fVolume)
	, m_fPitch(o.m_fPitch)
{

}

inline const COptions::CVoice&
COptions::CVoice::operator =(const CVoice &o)
{
	m_sEng = o.m_sEng;
	m_sLcl = o.m_sLcl;
	m_sVoice = o.m_sVoice;
	m_fRate = o.m_fRate;
	m_fVolume = o.m_fVolume;
	m_fPitch = o.m_fPitch;
	return *this;
}

inline int COptions::CVoice::cmp(const CVoice &v) const
{
	int c = strcmp(m_sEng, v.m_sEng);
	if (c == 0 && (c = strcmp(m_sLcl, v.m_sLcl)) == 0)
		c = strcmp(m_sVoice, v.m_sVoice);
	return c;
}

inline int COptions::CVoice::scmp(const CVoice &a, const CVoice &z)
{
	int c = strcmp(a.m_sEng, z.m_sEng);
	if (c == 0 && (c = strcmp(a.m_sLcl, z.m_sLcl)) == 0)
		c = strcmp(a.m_sVoice, z.m_sVoice);
	return c;
}

inline bool COptions::CVoice::IsEmpty() const
{
	return (!m_fPitch && !m_fRate && !m_fVolume && !m_sVoice.GetCount() && !m_sEng.GetCount() && !m_sLcl.GetCount());
}

inline COptions::CVoice&
COptions::CVoice::operator =(CVoice &&o)
{
	m_sEng.Attach(o.m_sEng);
	m_sLcl.Attach(o.m_sLcl);
	m_sVoice.Attach(o.m_sVoice);
	m_fRate = o.m_fRate;
	m_fVolume = o.m_fVolume;
	m_fPitch = o.m_fPitch;
	return *this;
}

inline int32_t COptions::SetBinData(CBYTE *cpb, int32_t cnt)
{
	int32_t i = m_av.SetBinData(cpb, cnt);
	i += m_sfp.SetBinData(cpb + i, cnt - i);
	return (i + glb::read_raw((BYTE*) &m_iCur, sizeof(int) * 2, cpb + i, cnt - i));
}

inline int32_t COptions::GetBinData(glb::CFxArray<BYTE, true> &ab)
{
	int32_t n = ab.GetCount();
	m_av.GetBinData(ab);
	m_sfp.GetBinData(ab);
	ab.AddND((CBYTE*) &m_iCur, sizeof m_iCur * 2);
	return (ab.GetCount() - n);
}

inline const COptions::CVoice& COptions::CurVoice() const
{
	ASSERT(m_iCur >= 0 && m_iCur < m_av.GetCount());
	return m_av[m_iCur];
}

inline int32_t COptions::FindVoice(const CVoice &v) const
{
	return glb::FindEqual<COptions::CVoice>(m_av, m_av.GetCount(), v, COptions::CVoice::scmp);
}

inline bool COptions::IsThereAnyVoice() const
{
	ASSERT(!m_av.GetCount() && m_iCur < 0 || m_iCur >= 0 && m_iCur < m_av.GetCount());
	return (m_iCur >= 0 && m_iCur < m_av.GetCount());
}

inline bool COptions::IsThereAFiletoRead() const
{
	return (m_sfp.GetCount() > 0);
}

#endif // COPTIONS_H
